# My Machine Manager 🧙✨⚙️ ✨💻 ✨🖥️

These are Ansible tasks to setup and manage my personal computers and help me keep them in sync.

In essence, these Ansible tasks:
- install/update software and tools (for my dev environment and for personal things)
- support Debian (both Buster and Bookworm) and MacOS
- aim to be straight forward, readable, and reusable

For installing things on Debian my preference is:
1. APT or AppImage
2. Flatpak

If there is something I need to install or do to one of my machines:
- Automate it via an Ansible task
- If I can't figure out how, leave myself some notes on how to do it manually

If you like the idea of automating your setup, feel free to copy or use anything as inspiration. ✌️❤️🌞
