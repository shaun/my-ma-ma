# A Docker container to test Ansible tasks

FROM debian:bullseye
WORKDIR /ansible-playground

RUN apt-get update && \
    apt-get install -y python3-apt && \
    apt-get install -y ansible

ADD . /ansible-playground

CMD ["ansible-playbook", "main.yml"]
