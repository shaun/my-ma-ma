## Disable Docker usage statistics

- Docker Desktop Settings
- General
- Uncheck "Send usgage statistics"

## Remap Cap Lock key to Control key

- System Preferences
- Keyboard
- Modifier Keys
- Caps Lock Key -> Control
